package main

import (
	"encoding/json"
	"log"
	"os"
	"sort"
	"sync"
	"time"

	fiber "github.com/gofiber/fiber/v2"
	"github.com/gofiber/swagger"
	_ "gitlab.com/Nikovar/temperature-forecast/docs"
	"gitlab.com/Nikovar/temperature-forecast/internal"
	"gopkg.in/natefinch/lumberjack.v2"
)

var (
	cities = []string{"London", "Moscow", "Tokyo", "Jakarta", "Chongqing",
		"Delhi", "Shanghai", "Troy", "Mumbai", "Manila",
		"Beijing", "Lahore", "Bangalore", "Paris", "Nagoya",
		"Chicago", "Madrid", "Dallas", "Miami", "Washington"}
	apiKey string
)

// @title NeForecast
// @version 0.1.3
// @description Api for Forecasting Service
// @contact.name API Support
// @contact.email fiber@swagger.io
// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @host localhost:666
// @BasePath /
func main() {
	var config = internal.Config
	err := config.GetConfig("settings.yml")
	if err != nil {
		log.Println(err)
	}
	internal.SetPool(config.User.Login, config.User.Password, config.User.Dbname) //"postgres", "2495234", "temperatures")
	if err != nil {
		log.Println(err)
		os.Exit(0)
	}

	apiKey = config.Api.Token

	err = reload(apiKey)
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}

	app := fiber.New(fiber.Config{
		BodyLimit:    64 * 1024 * 1024,
		ServerHeader: "Web-forecast",
		AppName:      "Web-forecast",
	})

	log.SetOutput(&lumberjack.Logger{
		Filename:   "log.txt",
		MaxSize:    50,
		MaxBackups: 2,
		MaxAge:     28,
		Compress:   true,
	})

	app.Get("/swagger/*", swagger.HandlerDefault)
	app.Get("/GetTemp", getTemperatures)
	app.Get("/GetCities", getCities)

	go CustomTrigger()
	log.Println(app.Listen("localhost:666"))
}

// GetTemp godoc
// @Summary      Show temperature
// @Description  get temperature by city
// @Tags         temperature
// @Produce		 json
// @Param        city    query     string  true  "Name of City"
// @Param        date    query     string  false  "Exact Date"
// @Success      200  {string} string
// @Router       /GetTemp [get]
func getTemperatures(c *fiber.Ctx) error {
	var result []byte
	var err error
	// db, err := internal.Connect("postgres", "2495234", "temperatures")
	// if err != nil {
	// 	return c.Status(401).SendString(err.Error())
	// }
	city := c.Query("city")
	date := c.Query("date")
	if date == "" {
		result, err = internal.SelectCity(internal.DbLocal, city, apiKey) //db, city, "c495400dc7433bd304af094089ec6335")
		if err != nil {
			return c.Status(400).SendString(err.Error())
		}
	} else {
		result, err = internal.SelectCityDate(internal.DbLocal, city, date, apiKey) //db, city, date, "c495400dc7433bd304af094089ec6335")
		if err != nil {
			return c.Status(400).SendString(err.Error())
		}
	}
	return c.Status(200).Send(result)
}

// GetCities godoc
// @Summary      Get all cities
// @Description  Get string of slice oof cities
// @Tags         city
// @Produce		 json
// @Success 200 {string} string
// @Router /GetCities 	[get]
func getCities(c *fiber.Ctx) error {
	result := append([]string{}, cities...)
	sort.Slice(result, func(i, j int) bool {
		return result[i] < result[j]
	})
	res, err := json.Marshal(result)
	if err != nil {
		log.Println(err)
		return err
	}
	return c.Status(200).Send(res)
}

func reload(apiKey string) error {
	var (
		wt sync.WaitGroup
	)
	results := make(chan internal.City, len(cities))

	for _, city := range cities {
		wt.Add(1)
		go func(city string, res chan internal.City, wt *sync.WaitGroup) {
			received, err := internal.RequestCity(city, apiKey, internal.DbLocal)
			if err != nil {
				log.Println(err)
			}

			results <- received

			wt.Done()
			return
		}(city, results, &wt)
	}

	wt.Wait()
	close(results)

	for val := range results {
		//log.Println(val)
		wt.Add(1)
		go func(city internal.City, wt *sync.WaitGroup) {
			internal.RequestInsert(city, apiKey, internal.DbLocal)
			wt.Done()
			return
		}(val, &wt)
	}
	wt.Wait()
	return nil
}

func CustomTrigger() {
	for {
		select {
		case <-time.Tick(1 * time.Minute):
			reload(apiKey)
			//log.Println("Reloaded in time")
		}
	}
}
