package main

import (
	//"database/sql"
	// "encoding/json"

	"log"
	"testing"

	_ "github.com/lib/pq"
	"gitlab.com/Nikovar/temperature-forecast/internal"
)

// func TestSelectCity(t *testing.T) {
// 	var config = internal.Config
// 	err := config.GetConfig("settings.yml")
// 	if err != nil {
// 		log.Println(err)
// 	}

// 	apiKey := config.Api.Token
// 	db, err := internal.Connect(config.User.Login, config.User.Password, "temperatures")
// 	if err != nil {
// 		log.Println(err)
// 		t.Errorf("Can not connect to database %v", err)
// 	}

// 	tests := []struct {
// 		City     string
// 		expected internal.City
// 	}{
// 		{
// 			City:     "TestCity",
// 			expected: internal.City{},
// 		},
// 	}
// 	for _, values := range tests {
// 		val, err := internal.SelectCity(db, values.City, apiKey)
// 		if err != nil {
// 			t.Errorf("%v", err)
// 		}
// 		t.Logf("%v", val)
// 	}
// }

func TestInsert(t *testing.T) {
	var config = internal.Config
	err := config.GetConfig("settings.yml")
	if err != nil {
		log.Println(err)
	}

	apiKey := config.Api.Token

	db, err := internal.Connect(config.User.Login, config.User.Password, "temperatures")
	if err != nil {
		log.Println(err)
		t.Errorf("Can not connect to database %v", err)
	}

	tests := []struct {
		Date     string
		Temp     float64
		Data     string
		City     string
		expected string
	}{
		{
			Date:     "0000-01-01 00:00:00",
			Temp:     -999.0,
			Data:     "{\"some\": 111}",
			City:     "TestCity",
			expected: "{\"some\": 111}",
		},
	}
	for _, value := range tests {
		err = internal.Insert(db, []byte(value.Data), value.City,
			internal.Forecast{Date: value.Date,
				Info: internal.Main{Temp: value.Temp}})
		if err != nil {
			t.Error(err)
		}
		received, err := internal.SelectCityDate(db, value.City, value.Date, apiKey)
		if err != nil {
			t.Error(err)
		}
		if string(received) != value.expected {
			t.Errorf("Received value are not equal to Inserted %v != %v", received, value.expected)
		} else {
			t.Log("Insert and Select test completed")
		}

	}

}
