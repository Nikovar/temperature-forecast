package internal

type config struct {
	Api struct {
		Token string `yaml:"token"`
	}
	User struct {
		Login    string `yaml:"login"`
		Password string `yaml:"password"`
		Dbname   string `yaml:"dbname"`
	}
}

type City struct {
	Name    string  `json:"name"`
	Country string  `json:"country"`
	Lat     float64 `json:"lat"`
	Lon     float64 `json:"lon"`
}

// type Info struct {
// 	Forecasts []Forecast `json:"list"`
// }

// type Info struct {
// 	Forecasts []map[string]interface{} `json:"list"`
// }

type Info struct {
	Forecasts []Forecast `json:"list"`
}

type InfoBytes struct {
	Forecasts []interface{} `json:"list"`
}

type Forecast struct {
	Date string `json:"dt_txt"`
	Info Main   `json:"main"`
}

type Main struct {
	Temp float64 `json:"temp"`
}

// HTTPError example
type HTTPError struct {
	Code    int    `json:"code" example:"400"`
	Message string `json:"message" example:"status bad request"`
}

type ReqBody struct {
	City string `json:"city"`
	Date string `json:"date"`
}
