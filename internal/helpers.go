package internal

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"gopkg.in/yaml.v2"
)

var Config config

func (c *config) GetConfig(configPath string) error {
	file, err := os.Open(configPath)
	if err != nil {
		return err
	}

	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(file)

	d := yaml.NewDecoder(file)

	if err := d.Decode(&c); err != nil {
		return err
	}
	//log.Print(c)
	return nil
}

func RequestCity(city, apiKey string, db *sql.DB) (City, error) {
	var data []City
	resp, err := http.Get("http://api.openweathermap.org/geo/1.0/direct?q=" + city + "&limit=1&appid=" + apiKey)
	if err != nil {
		log.Println(err)
		return City{}, err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return City{}, err
	}

	err = json.Unmarshal(body, &data)
	if err != nil {
		log.Println(err)
		return City{}, err
	}
	return data[0], nil
}

func RequestInsert(city City, apiKey string, db *sql.DB) error {
	var data Info
	var infoBytes InfoBytes
	lat := fmt.Sprintf("%f", city.Lat)
	lon := fmt.Sprintf("%f", city.Lon)
	resp, err := http.Get("http://api.openweathermap.org/data/2.5/forecast?lat=" + lat + "&lon=" + lon + "&appid=" + apiKey)
	if err != nil {
		log.Println(err)
		return err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return err
	}
	err = json.Unmarshal(body, &data)
	if err != nil {
		return err
	}
	err = json.Unmarshal(body, &infoBytes)
	if err != nil {
		return err
	}
	for i, forecast := range data.Forecasts {
		tmp, err := json.Marshal(infoBytes.Forecasts[i])
		if err != nil {
			return err
		}
		err = Insert(db, tmp, city.Name, forecast)
		if err != nil {
			return err
		}
	}
	//log.Println(data)
	return nil
}
