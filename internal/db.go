package internal

import (
	"database/sql"
	"encoding/json"
	"log"
	"sort"
	"sync"
	"time"

	_ "github.com/lib/pq"
)

var DbLocal *sql.DB
var mutex sync.Mutex

func SetPool(user, password, dbname string) error {
	var err error
	dbinfo := "user=" + user + " password=" + password + " dbname=" + dbname + " sslmode=disable"
	DbLocal, err = sql.Open("postgres", dbinfo)
	if err != nil {
		log.Println(err)
		return err
	}
	DbLocal.SetMaxOpenConns(5)
	return nil
}

func Create(db *sql.DB) error {
	statement := `
		CREATE TABLE tempperiod (date text,temperature double precision, other json,city text)`
	_, err := db.Exec(statement)
	if err != nil {
		log.Println(err)
		return err
	}

	return nil
}

func Connect(user, password, dbname string) (*sql.DB, error) {
	dbinfo := "user=" + user + " password=" + password + " dbname=" + dbname + " sslmode=disable"
	db, err := sql.Open("postgres", dbinfo)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	db.SetMaxOpenConns(5)
	return db, nil
}

func Insert(db *sql.DB, data []byte, city string, forecast Forecast) error { //forecast map[string]interface{}) error { //forecast Forecast) error {
	mutex.Lock()
	statement := `
		INSERT INTO TempPeriod (date, temperature, other,city)
		VALUES ($1, $2, $3,$4)
		ON CONFLICT (date,city) DO UPDATE
		SET date = EXCLUDED.date, city = EXCLUDED.city`

	_, err := db.Exec(statement, forecast.Date,
		forecast.Info.Temp, data, city,
	)
	mutex.Unlock()
	//log.Println(string(data))

	if err != nil {
		log.Println(err)
		return err
	}

	return nil
}

func SelectCity(db *sql.DB, city, apiKey string) ([]byte, error) {
	var dates []string
	var temperature float64
	var date string

	result := make(map[string]interface{})
	avTemp := 0.0
	counter := 0.0
	layout := "2006-01-02 15:04:05"
	now := time.Now()

	rows, err := db.Query("Select temperature,date from tempperiod where city ilike'" + city + "'")
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		err = rows.Scan(&temperature, &date)
		if err != nil {
			return nil, err
		}

		savedTime, err := time.Parse(layout, date)
		if err != nil {
			log.Println(err)
			return nil, err
		}

		if savedTime.Unix() > now.Unix() {
			dates = append(dates, date)
		}

		avTemp += temperature
		counter += 1.0
	}

	received, err := RequestCity(city, apiKey, db)
	if err != nil {
		return nil, err
	}

	sort.Slice(dates, func(i, j int) bool {
		return dates[i] < dates[j]
	})

	result["average_temperature"] = avTemp / counter
	result["city"] = city
	result["dates"] = dates
	result["country"] = received.Country

	bytesJson, err := json.Marshal(result)
	if err != nil {
		return nil, err
	}

	return bytesJson, nil
}

func SelectCityDate(db *sql.DB, city, date, apiKey string) ([]byte, error) {
	var result []byte
	var jsonB []byte

	rows, err := db.Query("Select other from tempperiod where city ilike'" + city + "' and date ilike '" + date + "'")
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		err = rows.Scan(&jsonB)
		if err != nil {
			return nil, err
		}
		result = jsonB
	}

	return result, nil
}
