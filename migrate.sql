CREATE DATABASE temperatures
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Russian_Russia.1251'
    LC_CTYPE = 'Russian_Russia.1251'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;


CREATE TABLE IF NOT EXISTS public.tempperiod
(
    date text COLLATE pg_catalog."default",
    temperature double precision,
    other json,
    city text COLLATE pg_catalog."default"
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tempperiod
    OWNER to postgres;
-- Index: pair

-- DROP INDEX IF EXISTS public.pair;

CREATE UNIQUE INDEX IF NOT EXISTS pair
    ON public.tempperiod USING btree
    (city COLLATE pg_catalog."default" ASC NULLS LAST, date COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;